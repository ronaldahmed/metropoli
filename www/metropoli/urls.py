from django.conf.urls import patterns, include, url

from django.contrib import admin
from website import views as website_views
from blog import views as blog_views
from venta import views as venta_views

admin.autodiscover()

handler400 = 'website.views.error_404'
handler403 = 'website.views.error_403'
handler404 = 'website.views.error_404'
handler500 = 'website.views.error_500'

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', website_views.Home.as_view(), name='index'),
    url(r'^servicios/$', website_views.ServiceList.as_view(), name='services'),

    url(r'^servicios/(?P<skudump>.*)/(?P<sku>.*)$',website_views.Render_SubService.as_view(), name='sub-service'),

    url(r'^servicios/(?P<sku>.*)$',website_views.Render_Service.as_view(), name='service'),
    url(r'^servicios/(?P<sku>.*)$',website_views.Render_Simple_Service.as_view()),

    url(r'^metropoli-tips/$',blog_views.BlogList.as_view(), name='blog'),
    url(r'^metropoli-tips/(?P<sku>.*)$',blog_views.BlogPost.as_view(), name='post'),

    url(r'^nosotros/$',website_views.nosotros, name='nosotros'),
    url(r'^contacto/$',website_views.contacto, name='contacto'),
    url(r'^contacto/gracias/$',website_views.thankyou, name='thankyou'),

    url(r'^compra-venta-inmuebles/$',venta_views.CompraVenta.as_view(), name='compra-venta'),
    url(r'^compra-venta-inmuebles/vendidos/$',venta_views.Vendidos.as_view(), name='vendidos'),
    url(r'^compra-venta-inmuebles/en-venta/$',venta_views.Lista_Inmuebles.as_view(), name='venta-list'),
    url(r'^compra-venta-inmuebles/en-venta/(?P<sku>.*)$',venta_views.Render_Inmueble.as_view(), name='venta'),

)

from django.conf import settings
if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),
    )

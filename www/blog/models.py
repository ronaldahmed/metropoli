from django.db import models
from django.template.defaultfilters import slugify

class Post(models.Model):
    title         = models.CharField(max_length = 160, blank = False)
    sku           = models.SlugField(max_length = 160, null=True, blank=True)  
    content       = models.TextField(max_length = 5000, blank = False)
    tags          = models.CharField(max_length = 100) # separador de tags ':'
    date          = models.DateField()
    list_img      = models.ImageField(upload_to='blog',blank=True)
    article_img   = models.ImageField(upload_to='blog',blank=True)
    
    def __unicode__(self):
        return u"%s - %s" % (self.title, self.sku)

    def url(self):
        return u"/metropoli-tips/%s" % self.sku
    
    def save(self,*args, **kwargs):
        '''Overwrite save method to send message_from_visitor signal,
        only when confirm was ticked on'''
        if not self.id:
            self.sku = slugify(self.title)
        super(Post, self).save(*args, **kwargs)
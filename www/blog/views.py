from django.shortcuts import render_to_response,redirect,render,get_object_or_404
from django.template import RequestContext, loader
from django.views.generic import TemplateView

from blog.models import Post
from website.models import Service, SubService, Simple_Service

class BlogList(TemplateView):
    template_name = 'blog.html'
    posts = Post.objects.all().order_by('-date')
    services = Service.objects.all()
    simple_services = Simple_Service.objects.all()

    def dispatch(self, *args, **kwargs):
        return super(BlogList, self).dispatch(*args, **kwargs)

    def get_context_data(self,  **kwargs):
        ctx= {'posts': self.posts,
              'services'   : self.services,
              'simple_services' : self.simple_services,
              }
        return ctx


class BlogPost(TemplateView):
    template_name = 'blog_post.html'
    posts = Post.objects.all().order_by('-date')
    services = Service.objects.all()
    simple_services = Simple_Service.objects.all()

    def dispatch(self, *args, **kwargs):
        return super(BlogPost, self).dispatch(*args, **kwargs)

    def get_context_data(self, sku,  **kwargs):
        tags = []
        post = ''
        #import pdb;  pdb.set_trace()
        try:
            post = get_object_or_404(Post,sku = sku)
            tags = [tag.strip(" ") for tag in post.tags.split(":")]
        except:
            post = None
            tags = []

        ctx= {'query_post': post,
              'posts': self.posts,
              'tags': tags,
              'services'   : self.services,
              'simple_services' : self.simple_services,
             }
        return ctx

import os
import sys
from django import template
from django.conf import settings
from django.utils.html import strip_tags
import unicodedata

register = template.Library()

@register.filter
def get_listimage_url(service,image_list):
    return image_list[service.sku].image.url
    
@register.filter
def getSubServices(service):
    return [serv for serv in service.subservice_set.all() if 'rest' not in serv.title.lower()]

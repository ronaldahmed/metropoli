from django.db import models
from django.template.defaultfilters import slugify

from django.conf import settings
import os

from metropoli.thumbs import ImageWithThumbsField

def getUploadPathService(instance, filename):
    '''
    :param instance: objeto modelo
    :param filename: de imagen
    :return: lo sube en carpeta con nombre del servicio
    '''
    path = '/'.join(['services',
                     slugify(instance.title_url),
                     filename,
                     ])

    real_path = os.path.join(settings.SITE_ROOT , os.path.join('media',os.path.dirname(path)) )
    if not os.path.exists(real_path):
        os.makedirs(real_path)
    return path


def getUploadPath_SubService(instance, filename):
    '''
    :param instance: objeto modelo
    :param filename: de imagen
    :return: lo sube en carpeta con nombre del servicio
    '''
    path = ''
    try:
        # instance: subservice_image
        path = '/'.join(['services',
                         slugify(instance.subservice.service.title_url),
                         slugify(instance.subservice.title_url),
                         filename,
                         ])
    except:
        #instance: subservice
        path = '/'.join(['services',
                         slugify(instance.service.title_url),
                         slugify(instance.title_url),
                         filename,
                         ])
    real_path = os.path.join(settings.SITE_ROOT , os.path.join('media',os.path.dirname(path)) )
    if not os.path.exists(real_path):
        os.makedirs(real_path)
    return path


def getUploadPath_SimpleService(instance,filename):
    path = '/'.join(['services/',
                     slugify(instance.service.title_url),
                     filename,
                     ])

    real_path = os.path.join(settings.SITE_ROOT , os.path.join('media',os.path.dirname(path)) )
    if not os.path.exists(real_path):
        os.makedirs(real_path)
    return path


##############################################################
##      MODELOS

## SERVICIO CON VARIOS SUB-SERVICIOS
class Service(models.Model):
    title       = models.CharField(max_length = 200, blank = False)
    title_url   = models.CharField(max_length = 200, blank = False)
    sku         = models.SlugField(max_length = 200, blank = True, null = True)
    content     = models.TextField(max_length = 1500, blank = False)
    mini_content = models.TextField(max_length = 1500, blank = False)
    mini_image  =  models.ImageField(upload_to = getUploadPathService,blank=False)


    def __unicode__(self):
        return u"%s - %s" % (self.title, self.sku)

    def url(self):
        return u"/servicios/%s" % self.sku

    def save(self,*args, **kwargs):
        '''Overwrite save method to send message_from_visitor signal,
        only when confirm was ticked on'''
        self.sku = slugify(self.title_url)
        super(Service, self).save(*args, **kwargs)


# SUB SERVICIOS
class SubService(models.Model):
    service = models.ForeignKey(Service)
    title       = models.CharField(max_length = 200, blank = False)
    title_url   = models.CharField(max_length = 200, blank = False)
    sku         = models.SlugField(max_length = 200, blank = True, null = True)
    content     = models.TextField(max_length = 1500, blank = False)
    mini_content = models.TextField(max_length = 1500, blank = True)

    mini_image  =  models.ImageField(upload_to = getUploadPath_SubService,blank=True)

    def __unicode__(self):
        return u"%s - %s" % (self.title, self.sku)

    def url(self):
        return u"/servicios/%s/%s" % (self.service.sku,self.sku)

    def save(self,*args, **kwargs):
        '''Overwrite save method to send message_from_visitor signal,
        only when confirm was ticked on'''
        self.sku = slugify(self.title_url)
        super(SubService, self).save(*args, **kwargs)


# IMAGENES DE CADA SUB SERVICIO
class SubServiceImage(models.Model):
    subservice = models.ForeignKey(SubService)
    description = models.TextField(max_length = 300, blank=True)
    image   = ImageWithThumbsField(upload_to=getUploadPath_SubService, sizes=((99,66),),blank=False)

    def __unicode__(self):
        return self.description


# SERVICIO SIMPLE - sin subservicios
class Simple_Service(models.Model):
    title       = models.CharField(max_length = 200, blank = False)
    title_url   = models.CharField(max_length = 200, blank = False)
    sku         = models.SlugField(max_length = 200, blank = True, null = True)
    content     = models.TextField(max_length = 1500, blank = False)
    mini_content = models.TextField(max_length = 1500, blank = False)
    mini_image  =  models.ImageField(upload_to = getUploadPathService,blank=False)


    def __unicode__(self):
        return u"%s - %s" % (self.title, self.sku)

    def url(self):
        return u"/servicios/%s" % self.sku

    def save(self,*args, **kwargs):
        '''Overwrite save method to send message_from_visitor signal,
        only when confirm was ticked on'''
        self.sku = slugify(self.title_url)
        super(Simple_Service, self).save(*args, **kwargs)


# IMAGENES DE SERVICIO SIMPLE
class Simple_ServiceImage(models.Model):
    service = models.ForeignKey(Simple_Service)
    description = models.TextField(max_length = 500, blank=True)
    image   = ImageWithThumbsField(upload_to=getUploadPath_SimpleService, sizes=((99,66),),blank=False)

    def __unicode__(self):
        return self.description
#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'ronald'
from django import forms
from django.utils.translation import ugettext_lazy as _
from website.models import Service, SubService, Simple_Service


class ContactForm(forms.Form):
    sub_services = SubService.objects.all()
    simple_services = Simple_Service.objects.all()
    all_services = [ss for ss in sub_services]
    all_services.extend([ss for ss in simple_services])

    SERV_CHOICES = [(ss.title_url,ss.title) for ss in all_services]
    SERV_CHOICES = [('DEF',"Seleccione un servicio")] + SERV_CHOICES + [('OTHER',"Otros")]

    name = forms.CharField(
        label="Nombre",
        max_length=50,
        error_messages={'blank': "Ingrese su nombre.",
                        'invalid': "Ingrese su nombre.",
                        'required': 'Ingrese su nombre.'}
    )
    email = forms.EmailField(
        label="E-mail",
        max_length=254,
        error_messages={'invalid': "Ingrese una cuenta de e-mail válida.".decode('utf8'),
                        'required': "Ingrese una cuenta de e-mail válida.".decode('utf8')}
    )
    phone = forms.CharField(
        label = "Teléfono".decode('utf8'),
        max_length = 15,
        error_messages = {'invalid': "Ingrese un número telefónico válido.".decode('utf8'),
                          'required': "Ingrese un número telefónico válido.".decode('utf8')},
        required = False,
        widget = forms.TextInput(attrs={'placeholder': 'celular o fijo'})
    )
    cotizacion = forms.ChoiceField(
        label="¿Desea una cotización?".decode('utf8'),
        choices=((True,'Si'),(False,'No')),
        initial=True,
        widget=forms.RadioSelect,
    )

    servicios = forms.ChoiceField(
        label = "Servicio a cotizar".decode('utf8'),
        choices = SERV_CHOICES,
        required = False,
    )

    comment = forms.CharField(
        label = "Detalles o consulta".decode('utf8'),
        max_length = 300,
        error_messages = {'blank': "Describa su consulta".decode('utf8'),
                          'required': "Ingrese su consulta"},
        widget = forms.Textarea(attrs={'placeholder': 'Descríbenos tu consulta'.decode('utf8'),
                                       'row': 4})
    )

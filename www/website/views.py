from django.shortcuts import render_to_response,redirect,render,get_object_or_404
from django.template import RequestContext, Context, loader
from django.views.generic import TemplateView
from django.http import HttpResponse,\
                        HttpResponseRedirect,\
                        HttpResponseForbidden,\
                        HttpResponseNotFound,\
                        HttpResponseServerError
from django.core.mail import send_mail, BadHeaderError
from django.conf import settings

from website.models import Service, SubService, Simple_Service
from venta.models import Inmueble
from blog.models import Post
from website.forms import ContactForm

import pdb

class Base(TemplateView):
    template_name = 'index.html'
    services = Service.objects.all()
    simple_services = Simple_Service.objects.all()
    posts = Post.objects.all()


class Home(Base):
    posts = Post.objects.all().order_by('-date')

    def dispatch(self, *args, **kwargs):
        return super(Home, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = {'services'   : self.services,
               'simple_services' : self.simple_services,
               'posts'      : self.posts[:3],
               }
        return ctx


class ServiceList(Base):
    template_name = 'base_service.html'

    #pdb.set_trace()

    def dispatch(self, *args, **kwargs):
        return super(ServiceList, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = {'services'   : self.services,
               'simple_services' : self.simple_services,
              }
        return ctx



class Render_Simple_Service(Base):
    template_name = 'service.html'  # por el momento

    #pdb.set_trace()

    def dispatch(self, *args, **kwargs):
        return super(Render_Simple_Service, self).dispatch(*args, **kwargs)

    def get_context_data(self, sku,  **kwargs):
        service = ''
        images = []
        try:
            service = get_object_or_404(Simple_Service,sku=sku)
            images  = service.simple_serviceimage_set.all()
        except:
            service = None

        ctx= {'services':self.services,
              'service': service,
              'subservices' : [],
              'simple_services' : self.simple_services,
              'images' : images,
             }
        return ctx


class Render_Service(Base):
    template_name = 'service.html'

    def dispatch(self, *args, **kwargs):
        return super(Render_Service, self).dispatch(*args, **kwargs)

    def get_context_data(self, sku,  **kwargs):
        service = ''
        images = []
        subservices = []
        try:
            service     = get_object_or_404(Service,sku=sku)
            subservices = service.subservice_set.all()
        except:
            temp = Render_Simple_Service()
            return temp.get_context_data(sku=sku)
            #service = None

        images = []
        for subs in subservices:
            images.extend(subs.subserviceimage_set.all())
        # quitar foo subservice rest

        subservices = [subs for subs in subservices if 'rest' not in subs.title]

        ctx= {'services':self.services,
              'service': service,
              'subservices' : subservices,
              'simple_services' : self.simple_services,
              'images' : images,
             }
        return ctx


class Render_SubService(Base):
    template_name = 'leave_service.html'

    def dispatch(self, *args, **kwargs):
        return super(Render_SubService, self).dispatch(*args, **kwargs)

    def get_context_data(self, skudump, sku,  **kwargs):
        service = ''
        images = []
        try:
            service = get_object_or_404(SubService,sku=sku)
        except:
            service = None

        images  = service.subserviceimage_set.all()
        parent_service = service.service
        ctx= {'services':self.services,
              'service': service,
              'simple_services' : self.simple_services,
              'images' : images,
              'parent' : parent_service,
             }
        return ctx

########################################################################################################################################

def nosotros(request):
    services = Service.objects.all()
    simple_services = Simple_Service.objects.all()
    return render_to_response('about.html', {'services'   : services,
                                             'simple_services' : simple_services})


def contacto(request):
    services = Service.objects.all()
    simple_services = Simple_Service.objects.all()

    if request.method == 'POST':
        form = ContactForm(data=request.POST)
        if form.is_valid():
            body = getMessageBody(form)
            subject = "Contacto cliente: " + form.cleaned_data['name']
            try:
                send_mail(subject,body,settings.DEFAULT_FROM_EMAIL,[settings.RECEIVER_MAIL])
            except BadHeaderError:
                return HttpResponse('Invalid header found.')
            return HttpResponseRedirect('/contacto/gracias/')
        else:
            return render_to_response('contacto.html',
                              {'form': form,
                               'services'   : services,
                               'simple_services' : simple_services},
                              context_instance=RequestContext(request))
    form = ContactForm()
    return render_to_response('contacto.html',
                              {'form': form,
                               'services'   : services,
                               'simple_services' : simple_services},
                              context_instance=RequestContext(request))


def getMessageBody(form):
    name = form.cleaned_data['name']
    email = form.cleaned_data['email']
    phone = form.cleaned_data['phone']
    cot = ''
    if form.cleaned_data['cotizacion']:
        cot = 'Si'
    else:
        cot = 'No'
    serv = form.cleaned_data['servicios']
    comment = form.cleaned_data['comment']

    body ='''Nombre: %s
E-mail :  %s
Telef  :  %s

Desea cotizacion  :  %s
Servicio a cotizar:  %s

Comentario:
  %s
''' % (name,email,phone,cot,serv,comment)

    #pdb.set_trace()

    return body


def thankyou(request):
    services = Service.objects.all()
    simple_services = Simple_Service.objects.all()
    return render_to_response('contact_gracias.html', {'services'   : services,
                                                       'simple_services' : simple_services})

########################################################################################################################################
## ERRORES
def error_403(request):
    t = loader.get_template('403.html')
    return HttpResponseForbidden(t.render(RequestContext(request)))


def error_404(request):
    t = loader.get_template('404.html')
    return HttpResponseNotFound(t.render(RequestContext(request)))


def error_500(request):
    t = loader.get_template('500.html')
    return HttpResponseServerError(t.render(RequestContext(request)))


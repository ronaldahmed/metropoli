from django.contrib import admin
from website.models import  Service,\
                            SubService,\
                            SubServiceImage,\
                            Simple_Service,\
                            Simple_ServiceImage

# Register your models here.

class Simple_ServiceImageInline(admin.TabularInline):
    model = Simple_ServiceImage
    extra = 3

class Simple_ServiceAdmin(admin.ModelAdmin):
    inlines = [Simple_ServiceImageInline,]


class Sub_ServiceImageInline(admin.TabularInline):
    model = SubServiceImage
    extra = 3

class Sub_ServiceAdmin(admin.ModelAdmin):
    inlines = [Sub_ServiceImageInline,]


admin.site.register(Service)
admin.site.register(SubService,Sub_ServiceAdmin)
admin.site.register(Simple_Service,Simple_ServiceAdmin)

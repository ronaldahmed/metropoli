from django.shortcuts import render_to_response,redirect,render,get_object_or_404
from django.template import RequestContext, loader
from django.views.generic import TemplateView
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseNotFound, HttpResponseServerError

from website.models import Service, SubService, Simple_Service
from venta.models import Inmueble, Vendido


class Lista_Inmuebles(TemplateView):
    template_name = 'venta_list.html'
    services = Service.objects.all()
    simple_services = Simple_Service.objects.all()
    inmuebles_venta = Inmueble.objects.all().order_by('-date')


    def dispatch(self, *args, **kwargs):
        return super(Lista_Inmuebles, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = {'services'   : self.services,
               'simple_services' : self.simple_services,
               'inmuebles'  : self.inmuebles_venta,
               }
        return ctx


class CompraVenta(Lista_Inmuebles):
    template_name = 'compra-venta.html'

    def dispatch(self, *args, **kwargs):
        return super(CompraVenta, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = {'services'   : self.services,
               'simple_services' : self.simple_services,
               }
        return ctx


class Vendidos(Lista_Inmuebles):
    template_name = 'vendidos.html'
    images = []

    def dispatch(self, *args, **kwargs):
        return super(Vendidos, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        images = Vendido.objects.all()

        ctx = {'services'   : self.services,
               'simple_services' : self.simple_services,
               'images' : images,
               }
        return ctx


class Render_Inmueble(Lista_Inmuebles):
    template_name = 'venta_inmueble.html'

    def dispatch(self, *args, **kwargs):
        return super(Render_Inmueble, self).dispatch(*args, **kwargs)

    def get_context_data(self, sku,  **kwargs):
        inmueble = ''
        images = []
        extras = []
        try:
            inmueble = get_object_or_404(Inmueble,sku=sku)
            images  = inmueble.inmuebleimage_set.all()
            extras = inmueble.extra_set.all()
        except:
            inmueble = None
            extras = []

        ctx= {'services':self.services,
              'simple_services' : self.simple_services,
              'inmueble': inmueble,
              'images' : images,
              'extras': extras,
             }
        return ctx


from django.db import models
from django.template.defaultfilters import slugify
from metropoli.thumbs import ImageWithThumbsField

from django.conf import settings
import os

def getUploadPathInmueble(instance, filename):
    '''
    :param instance: objeto modelo
    :param filename: de imagen
    :return: lo sube en carpeta con nombre del servicio
    '''
    try:
        # instance: inmueble
        path = '/'.join(['venta',
                         slugify(instance.title_url),
                         filename,
                         ])
    except:
        # instance: inmueble_image
        path = '/'.join(['venta',
                         slugify(instance.inmueble.title_url),
                         filename,
                         ])

    real_path = os.path.join(settings.SITE_ROOT , os.path.join('media',os.path.dirname(path)) )
    if not os.path.exists(real_path):
        os.makedirs(real_path)
    return path


class Inmueble(models.Model):
    title       = models.CharField(max_length = 300, blank = False)
    title_url   = models.CharField(max_length = 300, blank = False)
    sku         = models.SlugField(max_length = 300, blank = True, null = True)
    content     = models.TextField(max_length = 2000, blank = False)
    mini_content = models.TextField(max_length = 1500, blank = False)

    mini_image  =  models.ImageField(upload_to = getUploadPathInmueble,blank=False)

    address = models.CharField(max_length = 300, blank = False)
    price = models.CharField(max_length = 300, blank = True)
    date = models.DateField(auto_now=True)

    def __unicode__(self):
        return u"%s - %s" % (self.title, self.sku)

    def url(self):
        return u"/compra-venta-inmuebles/en-venta/%s" % self.sku

    def save(self,*args, **kwargs):
        '''Overwrite save method to send message_from_visitor signal,
        only when confirm was ticked on'''
        self.sku = slugify(self.title_url)
        super(Inmueble, self).save(*args, **kwargs)


# IMAGENES DE CADA INMUEBLE
class InmuebleImage(models.Model):
    inmueble = models.ForeignKey(Inmueble)
    description = models.TextField(max_length = 300, blank=True)
    image   = ImageWithThumbsField(upload_to=getUploadPathInmueble, sizes=((99,66),),blank=False)

    def __unicode__(self):
        return self.description


class Extra(models.Model):
    inmueble = models.ForeignKey(Inmueble)
    description = models.CharField(max_length=100, blank=False)

    def __unicode__(self):
        return self.description


class Vendido(models.Model):
    description = models.CharField(max_length = 300, blank = True)
    image   = ImageWithThumbsField(upload_to='vendidos/', sizes=((99,66),),blank=False)

    def __unicode__(self):
        return self.description
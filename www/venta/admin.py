from django.contrib import admin
from venta.models import  Inmueble,InmuebleImage, Extra, Vendido

# Register your models here.

class InmuebleImage_Inline(admin.TabularInline):
    model = InmuebleImage
    extra = 3

class Extra_Inline(admin.TabularInline):
    model = Extra
    extra = 3

class InmuebleAdmin(admin.ModelAdmin):
    inlines = [InmuebleImage_Inline,
               Extra_Inline,]


admin.site.register(Vendido)
admin.site.register(Inmueble,InmuebleAdmin)
